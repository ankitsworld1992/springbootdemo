package com.practise.bootdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practise.bootdemo.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
}
