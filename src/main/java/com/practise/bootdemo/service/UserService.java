package com.practise.bootdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practise.bootdemo.model.User;
import com.practise.bootdemo.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	 private UserRepository userRepository;
	
	public String saveUser(User user){
		
		User savedUser = userRepository.save(user);
		return savedUser!=null ? "SUCCESS" : "FAILED";
	}

}
