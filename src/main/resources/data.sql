DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  age INT NOT NULL
);

INSERT INTO users (name,age) VALUES
  ('TestUser1',11),
('TestUser2',15);