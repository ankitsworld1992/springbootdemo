package com.practise.bootdemo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.Mockito.when;
import com.practise.bootdemo.model.User;
import com.practise.bootdemo.repository.UserRepository;
import com.practise.bootdemo.service.UserService;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class UserServiceTest {
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserService userservice;
	
    User user = new User();
    
    
	 @BeforeEach
	    void setMockOutput() {
	        when(userRepository.save(user)).thenReturn(user);
	    }

	@Test
	public void testSaveUser(){
        assertEquals("SUCCESS",userservice.saveUser(user));

		
	}
	

}
