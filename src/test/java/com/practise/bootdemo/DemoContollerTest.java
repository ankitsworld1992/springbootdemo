package com.practise.bootdemo;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.practise.bootdemo.contoller.DemoController;


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DemoContollerTest {
	
	private MockMvc mvc;
	
	@Mock
	private RestTemplate restTemplate;
	
	
	 @InjectMocks
	 private DemoController demoController;
	
	@BeforeAll
	public void setup() {
        this.mvc = MockMvcBuilders.standaloneSetup(new DemoController()).build();

	}
	//https://howtodoinjava.com/spring-boot2/testing/spring-boot-mockmvc-example/
	
	@Test
	public void testDemoContoller() throws Exception 
	{
	  mvc.perform( MockMvcRequestBuilders
	      .get("/demo/fortest")
	      .contentType(MediaType.APPLICATION_JSON)
	      .accept(MediaType.APPLICATION_JSON))
	      .andExpect(status().isOk())
	      .andExpect(content().string("Hello world11"));
	}
	
	@Test
	public void testAnotherApiCall() {
		
		Mockito
        .when(restTemplate.getForEntity(
          "http://localhost:8080/spring/show",String.class))
        .thenReturn(new ResponseEntity("Data from other service", HttpStatus.OK));
        assertEquals("Data from other service",demoController.getResponseFromOtherService());

	}

}
