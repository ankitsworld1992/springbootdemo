package com.practise.bootdemo.contoller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.practise.bootdemo.model.User;
import com.practise.bootdemo.model.UserRequest;
import com.practise.bootdemo.service.UserService;

@RestController
@RequestMapping("/demo")
public class DemoController {
	
	Logger logger = LoggerFactory.getLogger(DemoController.class);
	
	@Autowired 
	Environment properties;
	
	@Autowired
	UserService userService;
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/display")
	public List<String> displyData(){
		
		return Arrays.asList("Text1","Text2");
	}
	
	@GetMapping("/showEnvrironmentVariable")
	public String showEnvVariable(){
		logger.info("Log message");
		return properties.getProperty("test.property");
		
	}
	
	@GetMapping("/fortest")
	public String fortest(){
		return "Hello world";
		
	}
	
	@GetMapping("/callanotherService")
	public String getResponseFromOtherService() {
		return restTemplate.getForEntity("http://localhost:8080/spring/show", String.class).getBody();
	}
	
	@PostMapping("/createUser")
	public String createUser(@RequestBody UserRequest userRequest){
	    User user = new User();
	    user.setAge(userRequest.getAge());
	    user.setUserName(userRequest.getName());
	    return userService.saveUser(user);
		
		
	}
}
